var app_id='ZmgXzA62FKaDlefJI94IW09G7';
var model='';
var device_token='';
var backbutton=0;
var set='';
var get_token='';

var app=angular.module('starter', ['ionic','chart.js','ngCordova','ionMDRipple','ui.rCalendar','ion-smooth-scroll'])

app.run(function($ionicPlatform,$cordovaNetwork,$cordovaDialogs,$localstorage,$state,acepush,$rootScope,$ionicLoading,$timeout,$ionicPopup) {
 $ionicPlatform.ready(function() {
      if(!localStorage.getItem('setting') || localStorage.getItem('setting') == null ){
      /*localStorage.setItem('setting',true);*/
       localStorage.setItem('setting',JSON.stringify(set));
    }

   if (window.cordova && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    cordova.plugins.Keyboard.disableScroll(true);
  }

  $rootScope.$on('loading:show', function() {
    var isAndroid = ionic.Platform.isAndroid();
    if(isAndroid){
      spinner='<ion-spinner icon="lines" class="spinner-royal"></ion-spinner>';
    }
    else{
      spinner='<ion-spinner icon="ios"></ion-spinner>';
    }

      $ionicLoading.show({  template: spinner,
          duration: 2000,animation: 'fadeIn',
                showDelay: 10})
    })

    $rootScope.$on('loading:hide', function() {
        $timeout(function () {
      $ionicLoading.hide();
    }, 1000);
    })

    document.addEventListener("offline", onOffline, false);
    function onOffline() {
     $cordovaDialogs.alert('Sorry, no Internet connectivity detected. Please reconnect and try again.', 'No Internet Connection', 'OK').then(function() {});
    }


 model=ionic.Platform.device();
   var push = PushNotification.init({ "ios": { "alert": "true", "badge": "true", "sound": "true", "clearBadge": true } });
  push.on('registration', function(data) {
//
// $ionicPopup.alert({
//     title: 'registration id',
//     template: JSON.stringify(data)
//   });

    $localstorage.set("Notification_token",data.registrationId);
    AppToken=data.registrationId;
    get_token=JSON.parse(localStorage.getItem('AppToken'));
    if(get_token != AppToken || !get_token || get_token=='' || get_token==null){
      localStorage.setItem('AppToken',JSON.stringify(AppToken));
      var obj={'app_id':app_id,'device_token':AppToken,'model':model.model,'platform':model.platform,'status':true}
        acepush.notification(obj);
        /*mylaipush.notification(obj);*/
    }
  });
  push.on('notification', function(data) {
    var a=data.additionalData.openUrl.split("_");

  });
  push.on('error', function(e) {
    $ionicPopup.alert({
        title: 'error',
        template: JSON.stringify(e)
      });
  });
})
});
app.filter('removeHTMLTags', function() {

	return function(text) {

		return  text ? String(text).replace(/<[^>]+>/gm, '') : '';

	};

});
app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$httpProvider,$provide) {
  $ionicConfigProvider.views.maxCache(0);
  $httpProvider.interceptors.push(function($rootScope) {

   return {
     request: function(config) {
       $rootScope.$broadcast('loading:show')
       return config
     },
     response: function(response) {
       $rootScope.$broadcast('loading:hide')
       return response
     }
   }
 })
 $ionicConfigProvider.views.swipeBackEnabled(false);
  $ionicConfigProvider.scrolling.jsScrolling(false);

  $stateProvider
// setup an abstract state for the tabs directive
.state('login', {
  url: '/login',
  templateUrl: 'templates/login.html',
  controller: 'LoginCtrl'
})
.state('menu', {
  url: '/menu',
  abstract: true,
  templateUrl: 'templates/menu.html',
  controller: 'AppCtrl'
})
.state('menu.dashboard', {
  url: '/dashboard',
  cache:false,
  views: {
    'menuContent': {
      templateUrl: 'templates/dashboard.html',
      controller: 'DashboardCtrl'
    }
  }
})
.state('menu.faculty', {
  url: '/faculty',
  cache:false,
  views: {
    'menuContent': {
      templateUrl: 'templates/faculty_page.html',
      controller: 'FacultyCtrl'
    }
  }
})
.state('menu.studentdetails', {
  url: '/studentdetails/:sid',
  cache:false,
  params: {obj:null},
  views: {
    'menuContent': {
      templateUrl: 'templates/studentdetails.html',
      controller: 'StudentDetailsCtrl'
    }
  }
})
.state('menu.details', {
  url: '/details/:sid',
  cache:false,
  params: {obj:null},
  views: {
    'menuContent': {
      templateUrl: 'templates/details.html',
      controller: 'DetailsCtrl'
    }
  }
});
// if none of the above states are matched, use this as the fallback
if(localStorage.getItem("token") == "" || localStorage.getItem("token") == null){
  $urlRouterProvider.otherwise('/login');
}
else if (localStorage.getItem("admin_role_id") == "4" ){

  $urlRouterProvider.otherwise('/menu/dashboard');

} else {
  $urlRouterProvider.otherwise('/menu/faculty');
}
});

app.factory('UrlService', function() {
  return {
    // check: 'http://aceclassroom.com/api/',
    check:'https://miraclehands.edu.lk/',
    // check: 'http://localhost:56653/api/',
  };
});

app.factory('ClockSrv', function($interval){
  var clock = null;
  var service = {
    startClock: function(fn){
      if(clock === null){
        // clock = $interval(fn, 15000);
      }
    },
    stopClock: function(){
      if(clock !== null){
        // $interval.cancel(clock);
        // clock = null;
      }
    }
  };
  return service;
})

function checkConnection() {
  var networkState = navigator.network.connection.type;
  var states = {};
  states[Connection.UNKNOWN]  = 'Unknown connection';
  states[Connection.ETHERNET] = 'Ethernet connection';
  states[Connection.WIFI]     = 'WiFi connection';
  states[Connection.CELL_2G]  = 'Cell 2G connection';
  states[Connection.CELL_3G]  = 'Cell 3G connection';
  states[Connection.CELL_4G]  = 'Cell 4G connection';
  states[Connection.CELL]     = 'Cell generic connection';
  states[Connection.NONE]     = 'No network connection';
  alert('Connection type: ' + states[networkState]);
}
