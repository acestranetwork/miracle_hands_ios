﻿app.controller('DashboardCtrl', function ($scope,$ionicLoading,SchoolInfo,$state,ajax,$cordovaDialogs,browser, $ionicHistory) {
  var School=SchoolInfo.get();
  $scope.image=School.SchoolLogo;
  $scope.schoolurl=School.SchoolUrl;
  $scope.id = localStorage.getItem("token");
  $scope.placeholder="img/placeholder.png";
  $scope.website = "http://www.miraclehands.lk/";
  $scope.result = {};
  var obj={ token : $scope.id };
  ajax.post(School.SchoolUrl+'api/aceConnect/Dashboard',obj).then(function(result) {
    console.log(result);
    if( result.UserValidation =="Success" )
    {
      $scope.AnnouncementList = result.AnnouncementList;
      $scope.StudentList = result.StudentList;
    }
    else if( result.Message =="Invalid User." )
   {
   $ionicHistory.clearCache("");
   $ionicHistory.clearHistory();
   $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
   $state.go('login');
   localStorage.setItem("UserInfo", "");
   localStorage.removeItem("UserInfo");
   localStorage.setItem("token", "");
   localStorage.removeItem("token");
   $cordovaDialogs.alert('Session Timeout', 'Miracle Hands', 'OK').then(function() {});
   }
 },function(error){
    if(error == 500 || error==404 || error == 0){
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'Miracle Hands', 'OK').then(function() {});
    }
  });
  $scope.studentdetails = function(sid){
    $state.go('menu.studentdetails',{obj:sid});
  };
   $scope.openBrowser = function(link) {
    browser.open(link);
  }
 $scope.colors=[];  for(var i=0; i<10;i++){    $scope.colors[i] = ('#'+ Math.floor(Math.random()*16777215).toString(16));  }
});
