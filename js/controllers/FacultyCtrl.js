﻿app.controller('FacultyCtrl', function ($scope,$ionicLoading,SchoolInfo,$state,ajax,$cordovaDialogs,browser, $ionicHistory) {
  var School=SchoolInfo.get();
  $scope.image=School.SchoolLogo;
  $scope.schoolurl=School.SchoolUrl;
  $scope.id = localStorage.getItem("token");
  $scope.website = "http://www.miraclehands.lk/";
  $scope.user_id = localStorage.getItem("user_id");
  $scope.faculty_name = localStorage.getItem("faculty_name");
  $scope.placeholder="img/placeholder.png"
  $scope.result = {};
  var obj={ user_id : $scope.user_id };
  console.log(obj);
  ajax.post(School.SchoolUrl+'api/aceConnect/student_leave_request',obj).then(function(result) {
    console.log(result);
    if( result.status =="success" )
    {

      $scope.StudentList = result.data;

      $scope.selected = [];
$scope.multicheck = function (user) {
  var index = $scope.selected.indexOf(user);
   if(index > -1) {
       $scope.selected.splice(index, 1);
       console.log($scope.selected);

   } else{
        $scope.selected.push(user);
console.log($scope.selected);

}
}

$scope.submit =function ()  {
  var data = [];

  angular.forEach($scope.selected, function(value, key){
    console.log(value);
    data.push({id:value.id,mt_id:value.mt_id});
  })

console.log(data);
  ajax.post(School.SchoolUrl+'api/aceConnect/student_leave_request_approve',{data:data}).then(function(result) {
    console.log(result);
    $cordovaDialogs.alert('Acknowledged Successfully', 'Miracle Hands', 'OK').then(function() {});
    location.reload();

  });

}

$scope.countChecked = function(){
    var count = 0;
    angular.forEach($scope.StudentList, function(value){
        if (value.Selected) count++;
    });

    return count;
}
$scope.cancel_toggle = function(){

  $scope.selectedAll = false;
  angular.forEach($scope.StudentList, function(value){
      value.Selected =  $scope.selectedAll;
  $scope.selectedAll = false;
  });
}

$scope.selectedAll = false;
$scope.checkAll = function () {
    if (!$scope.selectedAll) {
        $scope.selectedAll = true;
        angular.forEach($scope.StudentList, function (s) {
          var index = $scope.selected.indexOf(s);
           if(index > -1) {
               $scope.selected.splice(index, 1);
                s.Selected = $scope.selectedAll;
           } else {
                   $scope.selected.push(s);
                   console.log($scope.selected);
            s.Selected = $scope.selectedAll;
          }
        });
        console.log($scope.selectedAll);
    } else {
          $scope.selectedAll = false;
        angular.forEach($scope.StudentList, function (s) {
          var index = $scope.selected.indexOf(s);
          if(index > -1) {
              $scope.selected.splice(index, 1);

               s.Selected = $scope.selectedAll;

          } else{
  $scope.selected = [];

        $scope.selected.push(s);
        console.log($scope.selected);
 s.Selected = $scope.selectedAll;
}
      })

    }
};
    }
    else if( result.Message =="Invalid User." || result.Message =="Session Expired.")
   {
   $ionicHistory.clearCache("");
   $ionicHistory.clearHistory();
   $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
   $state.go('login');
   localStorage.setItem("UserInfo", "");
   localStorage.removeItem("UserInfo");
   localStorage.setItem("token", "");
   localStorage.removeItem("token");
   $cordovaDialogs.alert('Session Timeout', 'Miracle Hands', 'OK').then(function() {});
   }
 },function(error){
    if(error == 500 || error==404 || error == 0){
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'Miracle Hands', 'OK').then(function() {});
    }
  });
  $scope.studentdetails = function(sid){
    $state.go('menu.studentdetails',{obj:sid});
  };
   $scope.openBrowser = function(link) {
    browser.open(link);
  }
 $scope.colors=[];  for(var i=0; i<10;i++){    $scope.colors[i] = ('#'+ Math.floor(Math.random()*16777215).toString(16));  }
});
