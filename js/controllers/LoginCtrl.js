﻿app.controller('LoginCtrl', function ($scope,ajax,SchoolInfo,$state,UrlService,$ionicLoading,acepush,$cordovaDialogs,$ionicHistory,$localstorage,$ionicModal, $timeout) {
  angular.element(document.querySelector("ion-content")).removeClass("scroll-content");
  $scope.parentId = localStorage.getItem("UserID");
  $scope.password = localStorage.getItem("Pwd");
  this.password = "";
  $scope.clearvalue = function() {
    this.parentId = "";
    localStorage.setItem("UserID", "");
    localStorage.removeItem("UserID");
  }
  $scope.inputType = 'password';
  $scope.view = function(){
    $scope.inputType = $scope.inputType === "text" ? "password" : "text";
  };
// Create the login modal that we will use later
$ionicModal.fromTemplateUrl('templates/forgot.html', {
  scope: $scope
}).then(function(modal) {
  $scope.modal = modal;
});
// Triggered in the login modal to close it
$scope.close = function() {
  $scope.modal.hide();
  $scope.step1=true; $scope.step2=false;   $scope.step3=false;
};
// Open the login modal
$scope.reset = function() {
  $scope.modal.show();
};
$scope.step1=true; $scope.step2=false;   $scope.step3=false;
// Perform the login action when the user submits the login form
$scope.resetpwd = function(resetid) {
  if(!resetid){
    $cordovaDialogs.alert('Please enter Login Id', 'Miracle Hands', 'OK').then(function() {});
  }else{
        var obj1={parentId :resetid};
        ajax.post(UrlService.check+'api/aceConnect/Forgot',obj1).then(function(result) {
          console.log(result);
          $cordovaDialogs.alert(result.message, 'Miracle Hands', 'OK').then(function() {});

          var url = result.app_info;
          SchoolInfo.set({SchoolUrl:url.SchoolUrl,AppMenu:url.AppMenu,SchoolLogo:url.SchoolLogo,SchoolWebsite:url.SchoolWebsite});
          if(result.UserValidation =="Success"){
            $scope.step1=false; $scope.step2=true; $scope.step3=false;
            $scope.keycontent=result.message;
            localStorage.setItem("UserID",resetid);
          }else{
            $cordovaDialogs.alert(result.message, 'Miracle Hands', 'OK').then(function() {});
          }
        },function(error){
          if(error == 500 || error==404 || error == 0){
            $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'Miracle Hands', 'OK').then(function() {});
          }
        });
      }

}

$scope.verify=function(k1,k2,k3,k4){
  if(!k1 || ! k2 || !k3 || !k4){
    $cordovaDialogs.alert('Please enter verify code', 'Miracle Hands', 'OK').then(function() {});
  }else{
    var key=k1.toString()+k2.toString()+k3.toString()+k4.toString();
    var user=localStorage.getItem("UserID");
    var obj2={parentId :user,code:key};
    var School=SchoolInfo.get();
    ajax.post(UrlService.check+'api/aceConnect/Verify',obj2).then(function(result) {
      if(result.UserValidation =="Success"){
        $scope.step1=false; $scope.step2=false; $scope.step3=true;
        $scope.keycontent=result.message;
        console.log(result.message);
      }else{
        $cordovaDialogs.alert(result.message, 'Miracle Hands', 'OK').then(function() {});
        console.log(result.message);

      }
    },function(error){
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'Miracle Hands', 'OK').then(function() {});
      }
    });
  }
  this.key1='';this.key2='';this.key3='';this.key4='';
}
$scope.clean=function(){
  this.resetId='';
  this.key1='';this.key2='';this.key3='';this.key4='';
  this.newpassword='';
  this.confirmpassword='';
}
$scope.inputType1 = 'password';

$scope.hideShowPassword = function(){
 if ($scope.inputType1 == 'password')
   $scope.inputType1 = 'text';
 else
   $scope.inputType1 = 'password';
};
$scope.change=function(npwd,cpwd){
  if(!npwd || !cpwd){
    $cordovaDialogs.alert('Please enter all fields', 'Miracle Hands', 'OK').then(function() {});
  }else if(npwd != cpwd ){
    $cordovaDialogs.alert('Incorrect confirm password', 'Miracle Hands', 'OK').then(function() {});
  }else{
    var user=localStorage.getItem("UserID");
    var obj3={parentId :user,password:cpwd};
    var School=SchoolInfo.get();
    ajax.post(UrlService.check+'api/aceConnect/Reset',obj3).then(function(result) {
      if(result.UserValidation =="Success"){
        $cordovaDialogs.alert(result.message, 'Miracle Hands', 'OK').then(function() {});
        $scope.close();
      }else{
        $cordovaDialogs.alert(result.message, 'Miracle Hands', 'OK').then(function() {});
      }
    },function(error){
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'Miracle Hands', 'OK').then(function() {});
      }
    });
  }
}
$scope.login = function(parentId,password) {
  if(!parentId || !password){
    $cordovaDialogs.alert('Please enter all fields', 'Login failed!', 'OK').then(function() {});
  }else if(parentId.length <2){
    $cordovaDialogs.alert('Login ID minimum 2 characters required', 'Login failed!', 'OK').then(function() {});
  }else{
        var AppToken=$localstorage.get("Notification_token");
        var obj={ parentid : parentId , password : password , ntoken : AppToken , platform : "ios"};
        var obj1={'app_id':app_id,'device_token':AppToken,'model':model.model,'platform':model.platform,'status':true}
        ajax.post(UrlService.check+'api/aceConnect/Login',obj).then(function(res) {
          console.log(res);
           var result = res.user;
          if(res.status == "error"){
             acepush.notification(obj1);
            $cordovaDialogs.alert('Incorrect password', 'Login failed!', 'OK').then(function() {});
          }else if(res.status == "Incorrect username and password"){
            $cordovaDialogs.alert('Incorrect username and password', 'Login failed!', 'OK').then(function() {});
          }else {
               var url = res.app_info;
               SchoolInfo.set({SchoolUrl:url.SchoolUrl,AppMenu:url.AppMenu,SchoolLogo:url.SchoolLogo,SchoolWebsite:url.SchoolWebsite});
              if(result.admin_role_id == 4){
                localStorage.setItem("UserID",parentId);
                localStorage.setItem("Pwd",password);
                $ionicHistory.nextViewOptions({
                  disableAnimate: true,
                  disableBack: true
                });
                localStorage.setItem("token", result.login_token);
                localStorage.setItem("admin_role_id",result.admin_role_id);
                localStorage.setItem("UserID", parentId);
                $state.go('menu.dashboard',{});
                $scope.password = "";


            }else {
              $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
              });
              localStorage.setItem("token", result.login_token);
              localStorage.setItem("user_id", result.id);
              localStorage.setItem("UserID",parentId);
              localStorage.setItem("Pwd",password);
              localStorage.setItem("admin_role_id",result.admin_role_id);
              localStorage.setItem("faculty_name", result.name);
              $state.go('menu.faculty',{});
              $scope.password = "";


          }
          }
        },function(error){
          if(error == 500 || error==404 || error == 0){
            $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'Miracle Hands', 'OK').then(function() {});
          }
        });

}
}
});
